package com.example.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class BookDbHelper extends SQLiteOpenHelper {

    private static final String TAG = BookDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.

    private static SQLiteDatabase database;

    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "book.db";

    public static final String TABLE_NAME = "library";

    public static final String _ID = "_id";
    public static final String COLUMN_BOOK_TITLE = "title";
    public static final String COLUMN_AUTHORS = "authors";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_GENRES = "genres";
    public static final String COLUMN_PUBLISHER = "publisher";

    public BookDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        onCreate(database);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
/*
        db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        db.execSQL("CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " LONG NOT NULL, " +
                        COLUMN_BOOK_TITLE + " TEXT NOT NULL, " +
                        COLUMN_AUTHORS + " TEXT NOT NULL, " +
                        COLUMN_YEAR + " TEXT NOT NULL, " +
                        COLUMN_GENRES + " TEXT NOT NULL, " +
                        COLUMN_PUBLISHER + " TEXT NOT NULL) "
        );

        populate();*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


    }


   /**
     * Adds a new book
     * @return  true if the book was added to the table ; false otherwise
     */
    public boolean addBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
        long rowID = 0;

        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        rowID = numRows +1;

        ContentValues values = new ContentValues();
        values.put(_ID, rowID);
        values.put(COLUMN_BOOK_TITLE, book.getTitle());
        values.put(COLUMN_AUTHORS, book.getAuthors());
        values.put(COLUMN_YEAR, book.getYear());
        values.put(COLUMN_GENRES, book.getGenres());
        values.put(COLUMN_PUBLISHER, book.getPublisher());
        db.insert(TABLE_NAME, null, values);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a book inside the data base
     * @return the number of updated rows
     */
    public int updateBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
	    int res = 0;

        // updating row
        ContentValues values = new ContentValues();
        values.put(_ID, book.getId());
        values.put(COLUMN_BOOK_TITLE, book.getTitle());
        values.put(COLUMN_AUTHORS, book.getAuthors());
        values.put(COLUMN_YEAR, book.getYear());
        values.put(COLUMN_GENRES, book.getGenres());
        values.put(COLUMN_PUBLISHER, book.getPublisher());

        db.update(TABLE_NAME, values, "_id = "+book.getId(), null);
        return res;
    }


    public Cursor fetchAllBooks() {
        SQLiteDatabase db = this.getReadableDatabase();

	    Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME, null);
        
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public void deleteBook(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, _ID+"="+cursorToBook(cursor).getId(), null);
        db.close();
    }

    public void populate() {
        addBook(new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"));
        addBook(new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"));
        addBook(new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"));
        addBook(new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"));
        addBook(new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"));
        addBook(new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"));
        addBook(new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"));
        addBook(new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"));
        addBook(new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"));
        addBook(new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"));
        addBook(new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"));
    }

    public static Book cursorToBook(Cursor cursor) {

        Book book = new Book(
                cursor.getLong(cursor.getColumnIndexOrThrow(_ID)),
                cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_BOOK_TITLE)),
                cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_AUTHORS)),
                cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_YEAR)),
                cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_GENRES)),
                cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_PUBLISHER))
        );
        return book;
    }

    public Book getBookFromId(long id) {

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME+ " WHERE _id="+id, null);
        Book book = null;

        if (cursor != null) {
            cursor.moveToFirst();

            book = new Book(
                    cursor.getLong(cursor.getColumnIndexOrThrow(_ID)),
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_BOOK_TITLE)),
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_AUTHORS)),
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_YEAR)),
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_GENRES)),
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_PUBLISHER))
            );
        }

        return book;
    }
}
